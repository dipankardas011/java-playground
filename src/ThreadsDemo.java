import java.lang.*;
import java.util.Random;

public class ThreadsDemo {

    private int NO_OF_OPERATIONS = 5;
    public void caller() {
        Thread thread1 = new Thread(new Custom());
        Thread thread2 = new Thread(new Custom2());
        thread1.start();
        thread2.start();
    }

    public void Synchronizing() {
        for (int i = 0; i < NO_OF_OPERATIONS; i++) {
            String file = String.format("/home/xYz/state-%d.json",i);
            Thread state = new Thread(new StateStorage(file));
            Thread log = new Thread(new Logger(file));

            state.start(); // Start the StateStorage thread

            try {
                state.join(); // Wait for the StateStorage thread to complete
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            log.start(); // Start the Logger thread once StateStorage completes
        }

    }
}

class Custom extends Thread {
    public void run() {
        for (int i = 0; i < 5; i++) {
            System.out.println("Thread " + Thread.currentThread().getId() + ": " + i);        }
    }
}

class Custom2 extends Thread {
    public void run() {
        for (int i = 0; i < 5; i++) {
            System.out.println("Thread " + Thread.currentThread().getId() + ": " + i);        }
    }
}

class StateStorage extends Thread {
    private String path;
    public StateStorage(String path) {
        this.path = path;
    }
    public void run() {
        System.out.printf("[STARTED] State file '%s'\n", this.path);
        try {
            sleep(new Random().nextInt(2000, 10000));
            System.out.printf("[DONE] State file '%s'\n", this.path);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}

class Logger extends Thread {
    private String path;
    public Logger(String path) {
        this.path = path;
    }
    public void run() {
        System.out.printf("[SAVED] Saved the file '%s'\n",this.path);
    }
}